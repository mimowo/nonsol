# README #

### What is this repository for? ###

Simple and Fast Nonograms Solver

### How do I run it? ###

```
#!python
python nonsol.py <parameters>
```

parameters (order does not matter):
* -i <input file name> [default: examples/ex0.txt]

* -o <output file name> [default: not defined (stdout)]

* -a <y/n - ask to continue with every step> [default: n]

* -s <y/n - show progress (intermediate steps)> [default: y]

* -d <y/n - double characters in output> [default: n]

* -m <maximal number of line combinations> [default: 200000]

* -c <starting state input file name> [default: not defined]

* example: 


```
#!python 
python nonsol.py -i examples/ex0.txt -a n

```

Input file format description:
- first n lines provides description of n rows (from top to bottom and from left to right).
- one empty line (or with no numerical characters) to separate descriptions of rows and columns.
- next m line  provides description of m columns (from left to right and from top to bottom).

### Who do I talk to? ###

m.wozniak@mimuw.edu.pl