import time
import os, sys

print(os.path.abspath(os.curdir))
sys.path += [os.curdir + "/src/"]

from utils.obrazki_io import readCurrentObrazek, outObrazek, readObrazekDesc
from utils.obrazki_solve import solveObrazek
from utils.obrazki_general import QM

if sys.version.startswith("3"): raw_input = input

def parseParameters(sys_params):
    params = {"i":"examples/ex0.txt", "a": False, "s": True, "p":False, "m":200000, "d":False}
    for i in range(len(sys_params)-1):
        key_str = sys_params[i]
        value_str = sys_params[i+1]
        
        if key_str.startswith("-"): key_str = key_str[1:]
        else: continue
        
        if key_str in ["p", "s", "a", "d"]:
            if value_str.lower() == "y": params[key_str] = True
            else: params[key_str] = False
        elif key_str in ["m"]:
            params[key_str] = int(value_str)
        else:
            params[key_str] = value_str
    return params

def printHelp():
    help_text = "Simple Fast Nonograms Solver.\n\
No parameters or input file name provided.\n\
\n\
Running under python: python nonsol.py <parameters>\n\
\n\
parameters (order does not matter):\n\
-i <input file name> [default: examples/ex0.txt]\n\
-o <output file name> [default: not defined (stdout)]\n\
-a <y/n - ask to continue with every step> [default: n]\n\
-s <y/n - show progress (intermediate steps)> [default: y]\n\
-d <y/n - double characters in output> [default: n]\n\
-m <maximal number of line combinations> [default: 200000]\n\
-c <starting state input file name> [default: not defined]\n\
example: python nonsol.py -i examples/ex0.txt -a y\n\
\n\
Input file format description:\n\
- first n lines provides description of n rows (from top to bottom and from left to right).\n\
- one empty line (or with no numerical characters) to separate descriptions of rows and columns.\n\
- next m line  provides description of m columns (from left to right and from top to bottom)."

    print(help_text)
    ask = raw_input("Continue with default to solve the example (y/n)? [y - default]")
    if len(ask) >=1 and ask != "y":
        sys.exit()

    
if __name__ == '__main__':
    params = parseParameters(sys.argv)
    
    if len(sys.argv) < 2 or not "-i" in sys.argv:
        printHelp()
    
    input_fn = params["i"]
    if not os.path.exists(input_fn):
        print("Input file: " + input_fn +" does not exist.")
        print("Absolute path: " + os.path.abspath(input_fn))
        sys.exit()
        
    input_fh = open(input_fn)
    rows, columns = readObrazekDesc(input_fh)
    input_fh.close()
    
    n = len(rows)
    m = len(columns)
    obrazek = []
    
    for i in range(n): obrazek.append([QM]*m)
    
    if "c" in params:
        input_fn = params["c"]
        if not os.path.exists(input_fn):
            print("Input file: " + input_fn +" does not exist.")
            print("Absolute path: " + os.path.abspath(input_fn))
            sys.exit()
        input_fh = open(input_fn)
        obrazek = readCurrentObrazek(input_fh, obrazek)
        input_fh.close()

    s_time = time.time()
    obrazek = solveObrazek(obrazek, rows, columns, max_lines=params["m"], show_progress=params["p"], stop_steps=params["a"], show_steps=params["s"], show_double=params["d"])
    e_time = time.time()
    if not "o" in params:
        print(outObrazek(obrazek, params["d"]))
    else:
        output_fn = params["o"]
        output_fh = open(output_fn, "w")
        output_fh.write(outObrazek(obrazek, params["d"]))
        output_fh.close()
        
    print("Completed in time: " + str(e_time-s_time) + " sec.")
