from utils.obrazki_general import *

def readObrazekDesc(input_fh, dim_desc=False):
    lines = input_fh.readlines()
    rows = []
    columns = []
    if dim_desc:
        line0 = lines[0].strip().split("#")[0]
        line0 = line0.replace(",", " ")
        tokens = line0.split()
        n = int(tokens[0])
        m = int(tokens[1])
        for i in range(n):
            line = lines[i+1]
            tokens = line.split()
            int_tokens = []
            for token in tokens:
                int_tokens.append(int(token))
            rows.append(int_tokens)
        for j in range(m):
            line = lines[j+n+2]
            tokens = line.split()
            int_tokens = []
            for token in tokens:
                int_tokens.append(int(token))
            columns.append(int_tokens)
    else:
        add_rows = True
        for line in lines:
            line = line.strip().split("#")[0]
            
            if len(line) == 0:
                add_rows = False
                continue
            line = line.replace(",", " ")
            
            tokens = line.split()
            try:
                int_tokens = []
                for token in tokens: int_tokens.append(int(token))
                
                if add_rows: rows.append(int_tokens)
                else: columns.append(int_tokens)
            except:
                add_rows = False
    
    return rows, columns

def outObrazek(obrazek, double=False):
    n, m = len(obrazek), len(obrazek[0])
    out_text = ""
    for i in range(n):
        text_line = ""
        for j in range(m):
            if double:
                if obrazek[i][j] == QM: text_line += QM_c + QM_c
                elif obrazek[i][j] == XXX: text_line += XXX_c + XXX_c
                elif obrazek[i][j] == DOT: text_line += DOT_c + DOT_c
            else:
                if obrazek[i][j] == QM: text_line += QM_c
                elif obrazek[i][j] == XXX: text_line += XXX_c
                elif obrazek[i][j] == DOT: text_line += DOT_c
        text_line += "\n"
        out_text += text_line
    return out_text

def readCurrentObrazek(input_fh, obrazek):
    lines = input_fh.readlines()
    n = len(lines)
    for i in range(n):
        line = lines[i].strip()
        line_rev= convertLineRev(line)
        m = len(line)
        for j in range(m):
            obrazek[i][j] = line_rev[j]
        
    return obrazek
