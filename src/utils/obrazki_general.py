QM = 0
DOT =  1
XXX = 2
QM_c = "?"
DOT_c =  "."
XXX_c = "X"

def convertLine(line):
    c_map = {QM: QM_c, DOT:DOT_c, XXX:XXX_c}
    return ''.join(c_map.get(c, str(c)) for c in line)

def convertLineRev(line):
    c_map = {QM_c: QM , DOT_c:DOT, XXX_c:XXX}
    return [c_map.get(c, c) for c in line]

def convertList(list_tmp):
    return ''.join(str(n)+"," for n in list_tmp).strip(",")

def getObrazekRow(obrazek, i):
    return obrazek[i]

def getObrazekColumn(obrazek, j):
    column = []
    n = len(obrazek)
    for i in range(n):
        column.append(obrazek[i][j])
    return column

def setObrazekRow(obrazek, solved_line, i):
    m = len(obrazek[0])
    for j in range(m):
        obrazek[i][j] = solved_line[j]
    return obrazek

def setObrazekColumn(obrazek, solved_line, j):
    n = len(obrazek)
    for i in range(n):
        obrazek[i][j] = solved_line[i]
    return obrazek
