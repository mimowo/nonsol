import time
import sys
from utils.obrazki_general import *
from utils.obrazki_progress import ObrazkiProgress
from utils.obrazki_io import outObrazek

if sys.version.startswith("3"): raw_input = input

def possibleLinesRec(curr_line, k, line_desc, mem2=set([]), max_lines=1000):
    n = len(curr_line)

    if k == n and len(line_desc) == 0: return [curr_line]
    elif k == n: return []
    elif len(line_desc) - 1 + sum(line_desc) > n - k: 
        return []
    
    line_desc_txt = convertList(line_desc)
    if (k, line_desc_txt) in mem2: return [curr_line]
    
    if curr_line[k] == DOT: 
        ret_lines = possibleLinesRec(curr_line, k+1, line_desc, mem2=mem2, max_lines=max_lines)
        if len(ret_lines) > 0: mem2.add((k, line_desc_txt))
        return ret_lines
    elif curr_line[k] == XXX:
        if len(line_desc) == 0: return []
        else:
            curr_line_tmp = list(curr_line)
            curr_desc = line_desc[0]
            if k + curr_desc > n: return []
            for i in range(curr_desc):
                curr_line_tmp[k+i] = XXX
                if curr_line[k+i] == DOT: return []
            if k+curr_desc >= n:
                if len(line_desc) == 1: 
                    mem2.add((k, line_desc_txt))
                    return [curr_line_tmp]
                else: return []
            else:
                if curr_line[k+curr_desc] == XXX: return []
                curr_line_tmp[k+curr_desc] = DOT
                ret_lines = possibleLinesRec(curr_line_tmp, k+curr_desc+1, line_desc[1:], mem2=mem2, max_lines=max_lines)
                if len(ret_lines) > 0: mem2.add((k, line_desc_txt))
                return ret_lines
                
    else:
        curr_line_tmp1 = list(curr_line)
        curr_line_tmp1[k] = DOT
        all_lines1 = possibleLinesRec(curr_line_tmp1, k+1, line_desc, mem2=mem2, max_lines=max_lines)

        if len(line_desc) == 0: return all_lines1
        else:
            curr_line_tmp2 = list(curr_line)
            curr_desc = line_desc[0]
            if k + curr_desc > n: return []
            for i in range(curr_desc):
                curr_line_tmp2[k+i] = XXX
                if curr_line[k+i] == DOT: 
                    if len(all_lines1) > 0: mem2.add((k, line_desc_txt))
                    return all_lines1

            if k+curr_desc >= n:
                if len(line_desc) == 1: all_lines2 = [curr_line_tmp2]
                else: all_lines2 = []
            else:
                if curr_line[k+curr_desc] == XXX: 
                    if len(all_lines1) > 0: mem2.add((k, line_desc_txt))
                    return all_lines1
                curr_line_tmp2[k+curr_desc] = DOT
                all_lines2 =  possibleLinesRec(curr_line_tmp2, k+curr_desc+1, line_desc[1:], mem2=mem2, max_lines=max_lines)
                if len(all_lines2) + len(all_lines1) > max_lines: raise Exception("too many combs")
            
            if len(all_lines1) > 0 or len(all_lines2) > 9: mem2.add((k, line_desc_txt))
            return all_lines1 + all_lines2
            
    #print("here", k,n, curr_line, k, line_desc)
    return None


def generatePossibleLines(line_input, line_desc, max_lines = 1000):
    try:
        if len(line_desc) == 1 and line_desc[0] == 0:
            all_lines = [[DOT]*len(line_input)]
        else: all_lines = possibleLinesRec(line_input, 0, line_desc, mem2=set([]), max_lines = max_lines)
        if len(all_lines)==0:
            print("Not solvable!")
            sys.exit()
        if all_lines == None: 
            return None
        elif len(all_lines) > max_lines:
            print("Too many line combinations (more than m=" + str(max_lines) + ")")
            return None
    except:
        exc = sys.exc_info()[1]
        print("error", exc)
        return None
    return all_lines

def solveObrazekLine(line_input, line_desc, max_lines=20000):
    n = len(line_input)
    all_lines = generatePossibleLines(line_input, line_desc, max_lines=max_lines)
    if all_lines == None: 
        return line_input
    elif len(all_lines) == 0: return None
    else:
        line_solved = [QM]*n
        for i in range(n):
            if line_input[i] in [DOT, XXX]: line_solved[i] = line_input[i]
            else:
                char_sel = QM
                for line in all_lines:
                    char_tmp = line[i]
                    if char_sel == QM: char_sel = char_tmp
                    elif char_tmp == QM: continue
                    elif char_sel != char_tmp:
                        char_sel = QM
                        break
                line_solved[i] = char_sel
    return line_solved

def solveObrazek(obrazek, rows, columns, max_lines=20000, show_progress=True, stop_steps=False, show_steps=False, show_double=False):
    n, m = len(obrazek), len(obrazek[0])
    new_move = True
    mem_lines = {}
    iteration = 0
    while new_move:
        iteration += 1
        if show_progress:
            progress = ObrazkiProgress("Iteration " + str(iteration))
            progress.setJobsCount(n + m)
        s_time = time.time()
        new_move = False
        for i in range(n):
            if show_progress:
                progress.update("row: " + str(i))
                
            curr_line = getObrazekRow(obrazek, i)
            row = rows[i]
            curr_line_txt = convertLine(curr_line)
            row_txt = convertList(row)
            
            solved_line = mem_lines.get((curr_line_txt, row_txt), None)

            if solved_line == None:
                solved_line = solveObrazekLine(curr_line, row, max_lines=max_lines)
                mem_lines[(curr_line_txt, row_txt)] = solved_line
            
            if solved_line != None and solved_line != curr_line: 
                new_move = True
                obrazek = setObrazekRow(obrazek, solved_line, i)

        for j in range(m):
            if show_progress:
                progress.update("column: " + str(j))
            curr_line = getObrazekColumn(obrazek, j)
            column = columns[j]
            
            curr_line_txt = convertLine(curr_line)
            column_txt = convertList(column)
            
            solved_line = mem_lines.get((curr_line_txt, column_txt), None)

            if solved_line == None:
                solved_line = solveObrazekLine(curr_line, column, max_lines=max_lines)
                mem_lines[(curr_line_txt, column_txt)] = solved_line
            
            if solved_line != None and solved_line != curr_line:
                new_move = True
                obrazek = setObrazekColumn(obrazek, solved_line, j)
                    
        e_time = time.time()

        if show_steps:
            print(outObrazek(obrazek, double=show_double))
        if show_progress:
            print("Iteration "+str(iteration) + " completed in " + str(e_time-s_time) + " sec.")
        if stop_steps:
            ans = raw_input("Continue? (y[default]/n):")
            if len(ans) == 1 and ans.lower() == "n":
                sys.exit()
    return obrazek

